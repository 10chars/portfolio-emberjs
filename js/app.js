App = Ember.Application.create({});

/*FIXTURE DATA*/
var sites = [  
    {
        "id": 0,
        "title": "Leaving World",
        "sub": "English School",
        "thumbnail": "images/leaving-preview.jpg",
        "large": "leaving-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Single Page scroller, small business website.",
        "url": "http://www.leavingworld.com",
        "fullpreview": "leaving-full.jpg"
    },

    {
        "id": 1,
        "title": "Matsuo Shunji",
        "sub": "Personal Homepage",
        "thumbnail": "images/shunji-preview.jpg",
        "large": "shunji-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Personal Homepage.",
        "url": "http://www.matsuoshunji.jp",
        "fullpreview": "shunji-full.jpg"
    },

    {
        "id": 2,
        "title": "Creek",
        "sub": "Restaurant & Bar",
        "thumbnail": "images/creek-preview.jpg",
        "large": "creek-preview.jpg",
        "tags": "#SPA #wordpress #RESTAPI #bootstrap #AngularJS",
        "info": "Design and coding. Single Page Web Application.",   
        "url": "http://www.creek-hamamatsu.com/",
        "fullpreview": "creek-full.jpg"
    },

    {
        "id": 3,
        "title": "Tsumiki",
        "sub": "Bar",
        "thumbnail": "images/tsumiki-preview.jpg",
        "large": "tsumiki-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",        
        "info": "Design and coding. Info website for restaurant and bar.",
        "url": "http://luke-helg.sakura.ne.jp/tsumiki/",
        "fullpreview": "tsumiki-full.jpg"
    },

    {
        "id": 4,
        "title": "Diamond Berry",
        "sub": "Beauty Spa",
        "thumbnail": "images/diamond-preview.jpg",
        "large": "diamond-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Basic 5-page information website for Men's Health and Beauty Spa.",
        "url": "http://www.daimond-berry.com",
        "fullpreview": "diamond-full.jpg"
    }];

var about_text = [{ 
      "intro":"Hi, I'm Luke Helg.  I'm a developer from Auckland, New Zealand.  I have a BSc in Computer Science and have experience coding in a number of technologies. I been living in Japan for the past 5 years, currently doing freelance web design and development and am always on the look out for new projects to take on.",
      "hometownTitle":"Hometown",
      "hometown":"Auckland, New Zealand",
      "locationTitle": "Location",  
      "location":"Tokyo, Japan",
      "education":"BSc, Computer Science"
  }];

var contactText = [{
  name: 'Name',
  email: 'Email',
  subject: 'Subject',
  message: 'Message',
  send: 'SEND'

  }];

/* /FIXTURE DATA*/


App.Router.map(function() {
  this.resource('home');
  this.resource('contact');
  this.resource('about');
  this.resource('portfolio', { path: 'portfolio/:site_id' });

});

App.PortfolioRoute = Ember.Route.extend({
  model: function(params) {
    return sites.findBy('id', params.site_id);
  }
});

App.ContactRoute = Ember.Route.extend({
  model: function() {
    return contactText;
  }
});

App.AboutRoute = Ember.Route.extend({
  model: function(){
    return about_text;
  }
});

/*TODO: 
-Implement translation actions data. 
・put model in Controller, action to update returned model??  
・rootscope type lang variable in route??  
*/

App.HomeController = Ember.Controller.extend({
  greeting: 'Hello, my name is Luke.',
  intro: 'I make websites.',
  sites : [  
    {
        "id": 0,
        "title": "Leaving World",
        "sub": "English School",
        "thumbnail": "images/leaving-preview.jpg",
        "large": "leaving-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Single Page scroller, small business website.",
        "url": "http://www.leavingworld.com",
        "fullpreview": "leaving-full.jpg"
    },
    {
        "id": 1,
        "title": "Matsuo Shunji",
        "sub": "Personal Homepage",
        "thumbnail": "images/shunji-preview.jpg",
        "large": "shunji-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Personal Homepage.",
        "url": "http://www.matsuoshunji.jp",
        "fullpreview": "shunji-full.jpg"
    },
    {
        "id": 2,
        "title": "Creek",
        "sub": "Restaurant & Bar",
        "thumbnail": "images/creek-preview.jpg",
        "large": "creek-preview.jpg",
        "tags": "#SPA #wordpress #RESTAPI #bootstrap #AngularJS",
        "info": "Design and coding. Single Page Web Application.",   
        "url": "http://www.creek-hamamatsu.com/",
        "fullpreview": "creek-full.jpg"
    },
    {
        "id": 3,
        "title": "Tsumiki",
        "sub": "Bar",
        "thumbnail": "images/tsumiki-preview.jpg",
        "large": "tsumiki-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",        
        "info": "Design and coding. Info website for restaurant and bar.",
        "url": "http://luke-helg.sakura.ne.jp/tsumiki/",
        "fullpreview": "tsumiki-full.jpg"
    },
    {
        "id": 4,
        "title": "Diamond Berry",
        "sub": "Beauty Spa",
        "thumbnail": "images/diamond-preview.jpg",
        "large": "diamond-preview.jpg",
        "tags": "#jquery #wordpress #php #bootstrap",
        "info": "Design and coding. Basic 5-page information website for Men's Health and Beauty Spa.",
        "url": "http://www.daimond-berry.com",
        "fullpreview": "diamond-full.jpg"
    }],

    actions: {
      switchJP: function(){
        alert('TODO: Switch Language JP');
      },
      switchEN: function(){
        alert('TODO: Switch Language EN');
      }
  }

});







